![image](https://user-images.githubusercontent.com/15522554/150702960-85fdeb1c-938d-4014-902e-ebcf70d5b654.png)

# study-case

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
